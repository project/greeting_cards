var termSize = document.getElementById('terms');
var total = parseInt(termSize.getAttribute('data'));
var resultDiv = document.getElementById('search-result');
var currentItem = document.getElementById('current');


var loadingIcon = window.location.protocol+"//"+window.location.hostname+"/modules/custom/cards/assets/images/loading.gif";

console.log(loadingIcon)
jQuery("#edit-revision-log-wrapper").remove();
jQuery("#edit-langcode-wrapper").remove();

function sendResquest(tid){
  var xhr = new XMLHttpRequest();
  resultDiv.innerHTML = "<img src='"+loadingIcon+"'>";
  xhr.open('GET', 'greeting-card-search?load='+tid, true);
  xhr.setRequestHeader('Content-Type', 'application/json');
  xhr.onload = function (){
    if(this.status === 200){
      var data = JSON.parse(this.responseText);
      if(data.result.length !== 0){
        resultDiv.innerHTML = data.result;
      }else{
        resultDiv.innerHTML = "<p>Sorry we don't have cards of this category</p>";
      }
    }

  }
  xhr.send();

}

function currentSwitching(currentId = -1){
  var currentIndex = currentItem.getAttribute('data');
  if(currentId === -1){
    var c = document.getElementById('term-'+currentIndex);
    var l = document.getElementById('link-'+currentIndex);
    if(c !== null){
      var clasess = c.getAttribute('class');
      c.setAttribute('class', clasess+' active');
      l.setAttribute('class', 'text-white text-decoration-none');
    }
  }else{
    for (let i = 0; i < total; i++){
      var cc = document.getElementById('term-'+i);
      var ll = document.getElementById('link-'+i);
      if(cc !== null && ll !== null){
        var linkId = ll.id;
        if(linkId !== currentId){
          cc.removeAttribute('class');
          cc.setAttribute('class', 'name-item list-group-item');
          ll.removeAttribute('class');
          ll.removeAttribute('style');
        }else{
          cc.setAttribute('class', 'name-item list-group-item active');
          ll.setAttribute('class', 'text-white text-decoration-none');
          ll.style.color = 'black';
          ll.style.textDecoration = "none";
        }
      }
    }
  }

}

for(let i = 0; i < total; i++){
  var term = document.getElementById('term-'+i);

  if(term !== null){
    term.addEventListener('click', (e)=>{
      e.preventDefault();
      var tid = e.target.getAttribute('data');
      sendResquest(tid);

    });
  }
}

for(let j = 0; j < total; j++){
  var terlist = document.getElementById('term-'+j);

  if(terlist !== null){
    terlist.addEventListener('click', (e)=>{
      let id = e.target.id;
      if(id.includes("term"))
      {
        id = id.split("-");
        id = "link-"+ id[id.length - 1];
      }
      currentSwitching(id);
    });
  }
}

currentSwitching();
