<?php

namespace Drupal\greeting_cards\Form;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\file\Entity\File;
use Drupal\file\FileRepository;
use Drupal\file\FileStorage;
use Drupal\greeting_cards\Plugin\Logger;
use Drupal\greeting_cards\Plugin\PdfCreation;
use Drupal\node\NodeStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @file
 * E-Card uploading form.
 */

/**
 * To be used to upload cards and sending to friends.
 *
 * @class Uploading E-card form class.
 */
class EcardForm extends FormBase {

  /**
   * Pdf files created array.
   *
   * @var array
   */
  private array $pdfFiles;

  /**
   * Images passed an array.
   *
   * @var array
   */
  private array $images;

  /**
   * Construct for form to bring in dependencies.
   *
   * {@inheritdoc }
   */
  public function __construct(
    protected KillSwitch $killSwitch,
    protected NodeStorage $nodeStorage,
    protected FileStorage $fileSystem,
    protected $messenger,
    protected FileSystemInterface $system,
    protected FileRepository $fileRepository,
    protected Logger $logger
  ) {
  }

  /**
   * Build up dependencies.
   *
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):
  EcardForm|static {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('messenger'),
      $container->get('file_system'),
      $container->get('file.repository'),
      $container->get('greeting_cards.logger')
    );
  }

  /**
   * Return form id to FormBase.
   *
   * {@inheritdoc }
   */
  public function getFormId(): string {
    return "cards_uploading_form";
  }

  /**
   * Build form for uploading E-cards.
   *
   * {@inheritdoc }
   */
  public function buildForm(array $form,
                            FormStateInterface $form_state): array {

    $this->killSwitch->trigger();
    $form['fields'] = [
      "#type" => "fieldset",
      "#title"  => "E-Cards Creations",
      "#decription" => "To create E-Card for your friends and family.",
      "#attributes" => [
        'class' =>
        [
          'container',
          'w-50',
          'border',
          'rounded',
          'p-4',
        ],
      ],
    ];
    $form['fields']['card_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Card Title.'),
      '#required' => TRUE,
      '#description' => $this->t("Add title of you card."),
    ];

    $form['fields']['your_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Your Name.'),
      '#required' => TRUE,
      '#description' => $this->t("This name will be used in email which
      will be sent to your friend."),
    ];

    $form['fields']['your_relation'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Relationship.'),
      '#required' => TRUE,
      '#description' => $this->t("This relationship will be used in email
       which will be sent to your friend."),
    ];

    $form['fields']['occasion'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Occasion.'),
      '#required' => TRUE,
      '#description' => $this->t("This occasion will be used in email
       which will be sent to your friend."),
    ];

    $form['fields']['files'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload files'),
      '#upload_location' => 'public://cards/',
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#description' => $this->t('Allowed file types: pdf, jpg, jpeg, png.
       Note for all image files we will join them to make one pdf and
        make thumbnail of it then send to email provided.'),
      '#upload_validators' => [
        'file_validate_extensions' => ['pdf doc docx jpg jpeg png'],
        'file_validate_size' => [5 * 1024 * 1024],
        'file_validate_multiple' => [10],
      ],
    ];

    $form['fields']['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email'),
      '#required' => TRUE,
      '#description' => $this->t("Attach the email you want to send
       to this card preview Note that we are not saving the email used."),
    ];

    $form['fields']['taxonomy_autocomplete'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Category'),
      '#target_type' => 'taxonomy_term',
      '#required' => TRUE,
      '#selection_settings' => [
        'target_bundles' => ['card_categories'],
      ],
    ];

    $form['fields']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create and Send'),
      '#attributes' => [
        'class' => [
          'btn',
          'btn-secondary',
        ],
      ],
    ];

    return $form;
  }

  /**
   * Validation handler for Ecard.
   *
   * {@inheritdoc }
   */
  public function validateForm(array &$form,
                               FormStateInterface $form_state): void {
    parent::validateForm($form, $form_state);

    $value = $form_state->getValue('your_name');
    // You can add your custom validation logic for the 'Your Name' field here.
    if (strlen($value) < 3) {
      $form_state->setErrorByName("your_name", $this->t('Your Name
      must be at least 3 characters long.'));
    }

    $value = $form_state->getValue('your_relation');
    // You can add your custom validation logic for the 'Relationship'.
    if (strlen($value) < 3) {
      $form_state->setErrorByName("your_relation",
        $this->t('Relationship must be at least 3 characters long.'));
    }

    $value = $form_state->getValue('occasion');
    // You can add your custom validation logic for the 'Occasion' field here.
    if (strlen($value) < 3) {
      $form_state->setErrorByName("occasion",
        $this->t('Occasion must be at least 3 characters long.'));
    }

    $filesUploaded = $form_state->getValue("files");
    $email = $form_state->getValue("email");
    $taxonomy = $form_state->getValue("taxonomy_autocomplete");
    $card_title = $form_state->getValue("card_title");

    if (empty($card_title)) {
      $form_state->setErrorByName("card_title",
        "Title is not set");
    }

    if (empty($filesUploaded)) {
      $form_state->setErrorByName("files", "At least upload one
      file");
    }

    if (empty($taxonomy)) {
      $form_state->setErrorByName("taxonomy_autocomplete",
        "Select Category of card you want.");
    }

    if (empty($email)) {
      $form_state->setErrorByName("email", "Please provide email
       of receiver.");
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $form_state->setErrorByName("email", "Please provide valid
       email of receiver.");
    }
  }

  /**
   * Handle submitting of e cards.
   *
   * {@inheritdoc }
   */
  public function submitForm(array &$form,
                             FormStateInterface $form_state): void {
    $created = [];
    $filesUploaded = $form_state->getValue("files");
    $_SESSION['sender']['email'] = $form_state->getValue("email");
    $taxonomy = $form_state->getValue("taxonomy_autocomplete");
    $card_title = $form_state->getValue("card_title");
    $_SESSION['sender']['occasion'] = $form_state->getValue('occasion');
    $_SESSION['sender']['relation'] = $form_state->getValue('your_relation');
    $_SESSION['sender']['name'] = $form_state->getValue('your_name');

    // We have collected all images uploaded and create pdf from it.
    $pdf = $this->imagesTransformation($filesUploaded);

    // Take thumbnail of this pdf.
    if (!empty($pdf)) {
      // Making thumbnail.
      $thumbnail = greeting_cards_convert_pdf_to_image($pdf);

      // Saving pdf generated.
      $pdfId = $this->saveFile($pdf);

      // Create greeting card node.
      $created[] = $this->createNode(intval($pdfId),
        intval($taxonomy), intval($thumbnail), $card_title);
    }

    // Handle the pdf file uploaded.
    $pdfFiles = $this->handlePdfUploaded($filesUploaded);
    if (!empty($pdfFiles)) {
      foreach ($pdfFiles as $file) {
        $thumbnail = greeting_cards_convert_pdf_to_image($file['real']);
        $id = $file['id'];
        $created[] = $this->createNode(intval($id),
          intval($taxonomy), intval($thumbnail), $card_title);
      }
    }

    if (!in_array(0, $created)) {
      $form_state->setRedirect("greeting_cards.send_card_email", [
        'cards' =>
        implode(",", $created),
      ]);
    }
    else {
      $filteredArray = array_filter($created, function ($value) {
        return $value != 0;
      });
      if (count($filteredArray) > 0) {
        $form_state->setRedirect("greeting_cards.send_card_email", [
          'cards' =>
          implode(",", $filteredArray),
        ]);
      }
      else {
        if($this->messenger instanceof Messenger) {
          $this->messenger->addError('Failed to create cards') ;
        }
      }
    }
  }

  /**
   * This will only handle images.
   *
   * @param array $filesUploaded
   *   Array of uploads files.
   *
   * @return string
   *   File path of pdf created.
   */
  private function imagesTransformation(array $filesUploaded): string {
    foreach ($filesUploaded as $item) {
      if (is_numeric($item)) {
        $file = $this->fileSystem->load($item);
        if ($file instanceof File) {
          $list = explode("/", $file->getMimeType());
          $extension = end($list);
          $allowed = ['jpg', 'jpeg', 'png'];
          if (in_array(strtolower($extension), $allowed)) {
            $realFilePath = $this->system->realpath($file->getFileUri());
            $this->images[] = $realFilePath;
          }
        }
      }
    }
    if (!empty($this->images)) {
      $pdfFile = new PdfCreation($this->images);
      $pdfFile = $pdfFile->createPdfFile();
      return !empty($pdfFile) ? $pdfFile : "";
    }
    return "";
  }

  /**
   * File a path to save permanently.
   *
   * @param string $file
   *   File path.
   *
   * @return int|null
   *   Returns fid or null if failed.
   */
  private function saveFile(string $file): int|null {
    try {
      $info = new \SplFileInfo($file);
      restart:
      $filename = random_int(0, 2000000) . '.' . $info->getExtension();
      $destination = "public://$filename";
      if (file_exists($destination)) {
        goto restart;
      }
      $data = file_get_contents($file);
      if (!empty($data)) {
        $image = $this->fileRepository->writeData($data, $destination,
          FileSystemInterface::EXISTS_REPLACE);
        return $image->id();
      }
    }
    catch (\Throwable $throwable) {
      $this->logger->logger->error($throwable->getMessage());
      return NULL;
    }
    return NULL;
  }

  /**
   * Creation node of Greeting Cards.
   *
   * @param int $pdf
   *   Pdf id.
   * @param int $taxonomy
   *   Category term id.
   * @param int $thumbnail
   *   Thumbnail id.
   * @param string $title
   *   Card title.
   *
   * @return int
   *   Node id.
   */
  private function createNode(int $pdf,
                              int $taxonomy,
                              int $thumbnail,
                              string $title): int {
    try {
      if (!empty($thumbnail) && !empty($taxonomy) && !empty($pdf)) {
        $fields = [
          "type" => "greeting_cards",
          "title" => $title,
          "field_category" => [
            "target_id" => $taxonomy,
          ],
          "field_pdf" => [
            "target_id" => $pdf,
          ],
        ];

        if($pdf > 0) {
          $fields["field_thumbnail_image"] = [
            "target_id" => $thumbnail,
          ];
        }

        $node = $this->nodeStorage->create($fields);
        $node->save();
        return $node->id();
      }
    }
    catch (\Throwable $throwable) {
      $this->logger->logger->error($throwable->getMessage());
      return 0;
    }
    return 0;
  }

  /**
   * Handling uploaded files.
   *
   * @param mixed $filesUploaded
   *   Fids.
   *
   * @return array
   *   Array of pdf file uploaded.
   */
  private function handlePdfUploaded(mixed $filesUploaded): array {

    foreach ($filesUploaded as $item) {
      if (is_numeric($item)) {
        $file = $this->fileSystem->load($item);
        if ($file instanceof File) {
          $list = explode("/", $file->getMimeType());
          $extension = end($list);

          // Check only pdf.
          if (strtolower($extension) === "pdf") {

            // Realpath.
            $realFilePath = $this->system->realpath($file->getFileUri());
            $this->pdfFiles[] = ['real' => $realFilePath, 'id' => $item];
          }
        }
      }
    }

    return $this->pdfFiles ?? [];
  }

}

