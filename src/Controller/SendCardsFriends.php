<?php

namespace Drupal\greeting_cards\Controller;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileUrlGenerator;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileStorage;
use Drupal\greeting_cards\Plugin\EmailSender;
use Drupal\node\Entity\Node;
use Drupal\node\NodeStorage;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @file
 * Displaying created cards and sending one by one file.
 */

/**
 * Class to handle the displaying of cards created and sending.
 *
 * @class SendCardsFriends Class contains all functionalities for displaying.
 */
class SendCardsFriends extends ControllerBase {

  /**
   * Config interface for site info usage.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private ConfigFactoryInterface $config;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected readonly KillSwitch $killSwitch,
    protected readonly TermStorage $termStorage,
    protected readonly NodeStorage $nodeStorage,
    protected readonly FileStorage $fileSystem,
    protected readonly RendererInterface $renderer,
    protected $entityTypeManager,
    protected $messenger,
    ConfigFactoryInterface $configFactory,
    protected EmailSender $emailSender,
    protected $languageManager,
    protected FileUrlGenerator $fileUrlGenerator
  ) {
    $this->config = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):
  SendCardsFriends|static {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('renderer'),
      $container->get('entity_type.manager'),
      $container->get('messenger'),
      $container->get('config.factory'),
      $container->get('greeting_cards.email_sender'),
      $container->get('language_manager'),
      $container->get('file_url_generator')
    );
  }

  /**
   * Display Cards.
   *
   * @param \Symfony\Component\HttpFoundation\Request\Request $request
   *   Request object.
   *
   * @return array
   *   Returns cards array.
   */
  public function cardsDisplayer(Request $request): array {

    $this->killSwitch->trigger();

    // Collecting cards id 1,2,3,4.
    $cards = $request->get("cards");
    $data = [];

    // Collecting already sent cards ids.
    $alreadySent = $_SESSION['sent_already'] ?? [];
    if (!empty($cards)) {

      // Making a list of all cards made by this user.
      $cards = explode(",", $cards);

      // Extract cards still not sent.
      $cards = array_diff($cards, $alreadySent);

      // Build up an array for twig.
      foreach ($cards as $card) {
        if (is_numeric($card)) {
          $data[] = $this->card(intval($card), $cards);
        }
      }
    }
    return [
      '#theme' => 'displaying_e_cards_created',
      '#title' => 'Your Greeting Cards',
      '#content' => ['cards' => $data, "message" => "All Card emails sent."],
      '#attached' => [
        'library' => [
          'greeting_cards/manager_card',
        ],
      ],
    ];
  }

  /**
   * Sending Email to receiver.
   *
   * @param \Symfony\Component\HttpFoundation\Request\Request $request
   *   Requests object.
   *
   * @return string[]
   *   Invalid message if no error redirects back.
   */
  public function sendingEmails(Request $request): array {
    $this->killSwitch->trigger();

    // Getting received card to send email of and all cards.
    $card = $request->get("card");
    $cards = $request->get("cards");

    // Checking if we have valid card id.
    if (is_numeric($card)) {
      // Build up a card array from node.
      $data = $this->card(intval($cards), explode(',', $cards));

      // Making sure we have required data for mail sending.
      if (!empty($data['image']) && !empty($data['title'])) {

        // Building url of card.
        $cardUrl = Url::fromRoute("greeting_cards.show_all_card",
          ['nid' => $data['id']])
          ->toString();
        $cardUrl = $request->getSchemeAndHttpHost() . $cardUrl;

        // Sending mail.
        $result = $this->sendMail(
          $data['image'],
          $data['title'],
          $cardUrl,
          $request->getSchemeAndHttpHost()
        );

        // Building-up return link to all cards.
        $routeBack = Url::fromRoute("greeting_cards.send_card_email",
          ['cards' => $cards])->toString();

        // Checking if email sent or not for a proper feedback message.
        if ($result) {
          // If sent setting cards send id in a sent_already list.
          $_SESSION['sent_already'][] = $card;
          $this->messenger->AddMessage("Email sent successfully.");
        }
        else {
          $this->messenger->addError("Email sending failed.");
        }

        // Redirecting back to all cards.
        (new RedirectResponse($routeBack))->send();
        exit;
      }
    }
    return [
      "#markup" => "Card id is invalid",
    ];
  }

  /**
   * Sending mail to friend.
   *
   * @param string $image
   *   Image url thumbnail.
   * @param string $title
   *   Title of card.
   * @param string $url
   *   Url of card.
   * @param string $site
   *   This Site url.
   *
   * @return bool
   *   True if mail sent.
   */
  private function sendMail(
    string $image,
    string $title,
    string $url,
    string $site): bool {

    // Collecting submitted info for email to be sent.
    $senderName = $_SESSION['sender']['name'] ?? "Unknown name";
    $senderRelation = $_SESSION['sender']['relation'] ?? "Unknown relation";
    $receiverEmail = $_SESSION['sender']['email'] ?? NULL;
    $occasion = $_SESSION['sender']['occasion'] ?? "Unknown occasion";

    // Get the current Drupal site's name.
    $config = $this->config('system.site');
    $siteName = $config->get('name');

    // Get the site email address from the site configuration.
    $siteEmail = $config->get('mail');

    // Building up email body.
    $params['subject'] = "<p>Your Greeting Card is Ready on Our Website!";
    $params['message'] = "We hope this message finds you well.
                          We wanted to share some heartwarming news with you
                          - a special greeting card has been posted
                          on our website just for you! 🎉 <br><br>";
    $params['message'] .= "Your $senderName/$senderRelation has sent you a
                           thoughtful and heartfelt greeting card to celebrate
                           $occasion. We understand the
                           importance of cherished moments and wanted to make
                           this experience even more special for you. <br><br>";
    $params['message'] .= "<img src='$image' alt='$title'
                           style='max-width: 100%;'><br>";
    $params['message'] .= "To view and download your greeting card, click on the
                           link below: <br><br><a href='$url'>$title</a><br><br>";
    $params['message'] .= "We're sure you'll appreciate the kind words and
                           sentiments shared in the card. It's a wonderful way
                           to stay connected and share the joy of
                           $occasion even if you're miles apart.<br><br>";
    $params['message'] .= "Thank you for being a part of our community and for
                           allowing us to be a part of this meaningful moment
                           in your life. We hope you enjoy your greeting card,
                           and please feel free to reach out if you have any
                           questions or need assistance.<br><br>Wishing you all
                           the happiness and joy that $occasion brings!<br><br>";
    $params['message'] .= "Warm regards,<br><br>";
    $params['message'] .= "Customer Services<br>
                           $siteName<br>
                           $siteEmail<br>
                           $site";

    // Get the default language.
    $defaultLanguage = $this->languageManager->getDefaultLanguage();

    // Sending email.
    $result = $this->emailSender->sendEmail("greeting_cards", "greeting_cards_e_card", $receiverEmail,
      $defaultLanguage, $params);
    return !empty($result);
  }

  /**
   * Card build up.
   *
   * @param int $card
   *   Card id.
   * @param array $cards
   *   Cards list.
   *
   * @return array
   *   Card info array having image, title, node id, sendmail link.
   */
  private function card(int $card, array $cards): array {

    $node = $this->nodeStorage->load($card);

    // We are simplifying node data to just simple data for twig.
    if ($node instanceof Node) {

      // Getting thumbnail id.
      $image = $node->get("field_thumbnail_image")
        ->getValue()[0]['target_id'] ?? NULL;
      $url = NULL;

      // Checking if image id exist.
      if (!empty($image)) {

        // Loading file by image id.
        $file = $this->fileSystem->load($image);
        if ($file instanceof File) {
          $url = $file->getFileUri();
        }
      }

      // Creating link for email sending.
      $link = Url::fromRoute("greeting_cards.sending_mail",
        ['card' => $node->id(), 'cards' => implode(',', $cards)])
        ->toString();

      // Returning a card array.
      return [
        'id' => $node->id(),
        'title' => $node->getTitle(),
        'image' => $this->fileUrlGenerator->generateAbsoluteString($url),
        'sendlink' => $link,
      ];
    }
    return [];
  }

  /**
   * Displaying per card full details.
   *
   * @param \Symfony\Component\HttpFoundation\Request\Request $request
   *   Request Object.
   *
   * @return array
   *   Theme array.
   */
  public function cardVisited(Request $request): array {

    $this->killSwitch->trigger();

    $nid = $request->get("nid");
    // Placeholder.
    $data = [];

    // Checking if it numerical.
    if (is_numeric($nid)) {

      // Loading Node of nid.
      $node = $this->nodeStorage->load($nid);

      // Checking if node exists.
      if ($node instanceof Node) {

        // Node title.
        $data['title'] = $node->getTitle();
        $data['id'] = $node->id();

        $data['created'] = $this->ago(date("d-m-Y H:i:s",
          $node->getCreatedTime()));

        // Pdf file id.
        $pdf = $node->get("field_pdf")->getValue()[0]['target_id'] ?? NULL;

        // Category Id.
        $category = $node->get("field_category")->getValue()[0]['target_id'] ?? NULL;

        // Build up image.
        $card = $this->card($node->id(), []);
        $data['image'] = $card['image'] ?? NULL;

        // Build up pdf.
        if (!empty($pdf)) {
          $pdfInstance = $this->fileSystem->load($pdf);
          if ($pdfInstance instanceof File) {
            $data['pdf'] = $this->fileUrlGenerator->generateAbsoluteString($pdfInstance->getFileUri());
          }
        }

        // Build category.
        if (!empty($category)) {
          $taxonomy = $this->termStorage->load($category);
          if ($taxonomy instanceof Term) {
            $data['category'] = $taxonomy->getName();
          }
        }
      }
    }
    return [
      '#theme' => 'display_full_details_e_cards',
      '#title' => $data['title'],
      '#content' => [
        'card' => $data,
        'this_link' => $request->getSchemeAndHttpHost() . $request->getRequestUri(),
        "message" => "Card not found might have been deleted",
      ],
      '#attached' => [
        'library' => [
          'greeting_cards/manager_card',
        ],
      ],
    ];
  }

  /**
   * Calculate ago from datetime.
   *
   * @param string $datetime
   *   Date format time.
   * @param bool $full
   *   False or true.
   *
   * @return string
   *   Ago calculated.
   */
  private function ago(string $datetime, bool $full = FALSE): string {
    try {
      $now = new \DateTime();
      $ago = new \DateTime($datetime);
      $diff = $now->diff($ago);
      $diff->w = floor($diff->d / 7);
      $diff->d -= $diff->w * 7;
      $string = [
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
      ];
      foreach ($string as $k => &$v) {
        if ($diff->$k) {
          $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        }
        else {
          unset($string[$k]);
        }
      }

      if (!($full)) {
        $string = array_slice($string, 0, 1);
      }
      return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    catch (\Throwable $throwable) {
      return "unknown";
    }
  }

}
