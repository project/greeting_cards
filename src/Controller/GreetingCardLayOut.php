<?php

namespace Drupal\greeting_cards\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\file\FileStorage;
use Drupal\node\Entity\Node;
use Drupal\node\NodeStorage;
use Drupal\taxonomy\TermStorage;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @file
 * This file contains code that will display cards on /printable-cards page.
 */

/**
 * GreetingCard uses this class to handle cards display.
 *
 * @class building to display card and searching by tid and more.
 */
class GreetingCardLayOut extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected readonly KillSwitch $killSwitch,
    protected readonly TermStorage $termStorage,
    protected readonly NodeStorage $nodeStorage,
    protected readonly FileStorage $fileSystem,
    protected readonly AccountInterface $account,
    protected readonly RendererInterface $renderer,
    protected $entityTypeManager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container):
  GreetingCardLayOut|static {
    return new static(
      $container->get('page_cache_kill_switch'),
      $container->get('entity_type.manager')->getStorage('taxonomy_term'),
      $container->get('entity_type.manager')->getStorage('node'),
      $container->get('entity_type.manager')->getStorage('file'),
      $container->get('current_user'),
      $container->get('renderer'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Cards of thumbnail controller entry.
   *
   * @return array
   *   Cards with theme and libraries attachment.
   */
  public function displayCards(Request $request): array {
    $this->killSwitch->trigger();
    $query = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery();
    $query->condition('vid', "card_categories");
    $query->accessCheck(FALSE);
    $tids = $query->execute();
    $terms = $this->termStorage->loadMultiple($tids);

    $url = $request->getRequestUri();
    $url = str_contains($url, '?') ?
      substr($url, 0, strpos($url, '?')) : $url;

    $flag = FALSE;
    $firstTid = 0;
    $i = 0;
    $backupTid = [];

    $categories = [];

    foreach ($terms as $term) {
      $category = [];
      if ($flag === FALSE) {
        $firstTid = $term->id();
        $flag = TRUE;
      }
      $id = $term->id();
      $backupTid[] = $id;
      $total = count($this->loadByTid($id));
      $tt = $term->getName();
      $category['name'] = $tt;
      $category['tid'] = $id;
      $category['total'] = $total;
      $category['link_id'] = "link-$i";
      $category['div_id'] = "term-$i";
      $category['uri'] = $url . "?load=$id";
      $categories[] = $category;
      $i++;
    }
    $categories['total_terms'] = $i;

    if ($firstTid !== 0) {
      $nids = $this->loadByTid($firstTid);
      if (count($nids) === 0) {
        $j = 0;
        foreach ($backupTid as $t) {
          $n = $this->loadByTid($t);
          if (count($n) !== 0) {
            $firstTid = $t;
            break;
          }
          $j++;
        }
      }
    }
    $nids = $this->loadByTid($request->get('load') ??
      $firstTid);
    $nodes = $this->loadNodes($nids);
    $resultLoad = $this->mapNodeWithHtml($nodes);
    $output = [
      '#theme' => 'cards_displaying_cards',
      '#title' => 'Greeting Cards',
      '#content' => ['terms' => $categories, 'cards' => $resultLoad],
      '#attached' => [
        'library' => [
          'greeting_cards/manager_assets',
        ],
      ],
    ];
    return $output;
  }

  /**
   * Using $nids of greeting cards node to load all nodes.
   *
   * @param mixed $nids
   *   Ids of nodes of content type greeting cards.
   *
   * @return array
   *   Nodes of Greeting Cards are returned.
   */
  public function loadNodes(mixed $nids): array {
    $nodes = [];
    foreach ($nids as $nid) {
      if (!empty($nid)) {
        $node = $this->nodeStorage->load($nid);
        if ($node instanceof Node) {
          $nodes[] = $node;
        }
      }
    }
    return $nodes;
  }

  /**
   * Brings up nodes that associate with this tid passed.
   *
   * @param mixed $tid
   *   ID of category click on the display page.
   *
   * @return int|array
   *   All nodes will be return associated with tid or int of not results.
   */
  public function loadByTid(mixed $tid): int|array {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->condition('type', 'greeting_cards')
      ->condition('field_category', intval($tid))
      ->accessCheck(TRUE);
    return $query->execute();
  }

  /**
   * The nodes in such a way that will reduce transmitting large data.
   *
   * @param array $nodes
   *   Array of greeting cards to be used for buildup.
   *
   * @return array
   *   Array of well-formatted data of nodes passed.
   *
   * @throws \Exception
   */
  public function mapNodeWithHtml(array $nodes): array {
    $cards = [];
    foreach ($nodes as $node) {
      $thisCard = [];
      $thumbId = $node->get("field_thumbnail_image")
        ->getValue()[0]['target_id'] ?? -1;
      $pdf = $node->get("field_pdf")->getValue()[0]['target_id'] ?? -1;

      // Thumbnail.
      $file = $this->fileSystem->load($thumbId);
      if ($file instanceof File) {
        $thumbnailImage = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
        $imageDefaults = ['modules/contrib/greeting_cards/assets/images/noimage.avif',
          'modules/contrib/greeting_cards/assets/images/noimage2.png',
        ];
        if (empty($thumbnailImage)) {
          $thumbnailImage = $imageDefaults[random_int(0, count($imageDefaults) - 1)];
        }
      }
      else {
        $imageDefaults = ['modules/contrib/greeting_cards/assets/images/noimage.avif',
          'modules/contrib/greeting_cards/assets/images/noimage2.png',
        ];
        $thumbnailImage = $imageDefaults[random_int(0, count($imageDefaults) - 1)];
      }
      $thisCard['imageLink'] = $thumbnailImage;

      // Pdf.
      $file = $this->fileSystem->load($pdf);
      $pdfFile = "";
      if ($file instanceof File) {
        $pdfFile = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
      }
      $thisCard['pdfLink'] = $pdfFile;
      $thisCard['title'] = $node->title->value;
      $linkD = Url::fromUri("internal:/printable-cards");
      $nodeDeleteLink = "node/{$node->id()}/delete?destination=
      {$linkD->toString()}";
      $currentUser = $this->account->getRoles();
      $thisCard["deleteLink"] = $nodeDeleteLink;

      if ($this->isAllowedFormDisplay($currentUser)) {
        $thisCard['isAllowed'] = TRUE;
      }
      else {
        $thisCard['isAllowed'] = FALSE;
      }
      $cards[] = $thisCard;
    }
    return $cards;
  }

  /**
   * Json response to page will be returned.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Data will be associated with a key result
   *
   * @throws \Exception
   */
  public function searchResult(Request $request): JsonResponse {
    $tid = $request->get('load');
    if (!empty($tid)) {
      $nids = $this->loadByTid($tid);
      $nodes = $this->loadNodes($nids);
      $resultLoad = $this->mapNodeWithHtml($nodes);

      $rendered_output = [
        '#theme' => 'cards_results_search',
        '#title' => NULL,
        '#content' => ['cards' => $resultLoad],
        '#attached' => [
          'library' => [
            'greeting_cards/manager_assets',
          ],
        ],
      ];
      // Render the HTML output using the renderer service.
      $rendered_output = $this->renderer->renderRoot($rendered_output);

      return new JsonResponse([
        'result' => $rendered_output,
      ], Response::HTTP_OK);
    }
    return new JsonResponse([], Response::HTTP_NOT_FOUND);
  }

  /**
   * Check for allowed roles to have a delete link on card displayed.
   *
   * @param array $userRoles
   *   Array of users' roles website has.
   *
   * @return bool
   *   True for ok display link or false do display delete link
   */
  private function isAllowedFormDisplay(array $userRoles): bool {
    $allowed = ['administrator'];
    foreach ($allowed as $role) {
      if (in_array($role, $userRoles)) {
        return TRUE;
      }
    }
    return FALSE;
  }

}
