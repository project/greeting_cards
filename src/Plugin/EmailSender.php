<?php

namespace Drupal\greeting_cards\Plugin;

use Drupal\Component\Plugin\PluginManagerBase;
use Drupal\Core\Language\Language;

/**
 * @file
 * Service provider for mail.plugin.
 */

/**
 * EmailSender will be user to handle mail dependencies.
 *
 * @class Class that give mail service to controller.
 */
class EmailSender {

  /**
   * Mail plugin holder.
   *
   * @var \Drupal\Component\Plugin\PluginManagerBase
   */
  protected PluginManagerBase $mailManager;

  /**
   * Construct dependencies.
   *
   * {@inheritdoc }
   */
  public function __construct(PluginManagerBase $mailManager) {
    $this->mailManager = $mailManager;
  }

  /**
   * Sending mail to friends.
   *
   * @param string $module
   *   Module name.
   * @param string $key
   *   Mail hook key.
   * @param string $to
   *   Receiver emailAddress.
   * @param \Drupal\Core\Language\Language $langcode
   *   Default lang code.
   * @param array $params
   *   Params consist of a message and subject.
   *
   * @return bool
   *   Return true if sent.
   */
  public function sendEmail(
    string $module,
    string $key,
    string $to,
    Language $langcode,
    array $params
  ): bool {

    // Sending email.
    $result = $this->mailManager->mail($module, $key, $to, $langcode, $params);
    return !empty($result['result']);
  }

}
