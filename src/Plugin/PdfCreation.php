<?php

namespace Drupal\greeting_cards\Plugin;

use Mpdf\Mpdf;
use Mpdf\Output\Destination;

/**
 * @file
 * File contain class to generate pdf.
 */

/**
 * In this class, we are basically building image to pdfs.
 *
 * @class PdfCreation takes images to insert in pdf file.
 */
class PdfCreation {

  /**
   * Construct pdf.
   *
   * @param array $images
   *   Images path public://.
   */
  public function __construct(private array $images) {}

  /**
   * Generating pdf from html using images uploaded.
   *
   * @return string|null
   *   Returns a path of pdf or null if fails.
   */
  public function createPdfFile(): string|null {
    try {

      // Setting required configs for mpdf.
      $mpdf = new Mpdf(
        [
          'format' => 'A4-P',
          'tempDir' => 'sites/default/files/tmpmpdf',
        ]
      );
      $mpdf->AddPageByArray([
        'margin-left' => 5,
        'margin-right' => 5,
        'margin-top' => 5,
        'margin-bottom' => 5,
      ]);

      // Setting metadata for pdf to be created.
      $mpdf->SetAuthor("Chance Nyasulu");
      $mpdf->SetCreator("Greeting Cards module drupal");
      $mpdf->SetTitle("Greeting Card");
      $mpdf->SetKeywords("Chance, Sv, Drupal, Module, svinfotech");

      // Properly attaching images using img tag since we are creating pdf from.
      $imagesLine = "";
      foreach ($this->images as $image) {
        $imagesLine .= "<img src='$image'/>" . PHP_EOL;
      }

      // Template content html full.
      $template = "modules/contrib/greeting_cards/assets/pdf.html";
      if (file_exists($template)) {

        // Reading content of template and replace area {{IMAGESLINE}} with
        // images build above.
        $content = file_get_contents($template);
        $content = str_replace("{{IMAGESLINE}}", $imagesLine, $content);

        // Writing pdf file.
        $mpdf->WriteHTML(file_get_contents($this->getStyleSheet()), 1);
        $mpdf->WriteHTML($content, 2);
        $mpdf->SetJS('this.print();');

        // Saving pdf data into pdf file.
        up:
        $pdfFilePath = "public://cards/combined_" . random_int(0, 10000) . ".pdf";
        if (file_exists($pdfFilePath)) {
          goto up;
        }

        $mpdf->Output($pdfFilePath, Destination::FILE);
        return $pdfFilePath;
      }
    }
    catch (\Throwable $throwable) {
      \Drupal::logger("greeting_cads")->error($throwable->getMessage());
      return NULL;
    }
    return NULL;
  }

  /**
   * Return styles file path.
   *
   * @return string
   *   Styles file.
   */
  public function getStyleSheet(): string {
    return 'modules/contrib/greeting_cards/assets/css/styles.css';
  }

}
