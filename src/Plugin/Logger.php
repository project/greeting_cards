<?php

namespace Drupal\greeting_cards\Plugin;

use Drupal\Core\Logger\LoggerChannel;

/**
 * @file
 * Logger to error, notice for e_cards.
 */

/**
 * Logger Class for saving all need errors to logger.
 *
 * @class Logger will handle inject of logger to Controllers.
 */
class Logger {

  /**
   * Holder logger object.
   *
   * @var \Drupal\Core\Logger\LoggerChannel
   */
  public LoggerChannel $logger;

  /**
   * Construct of this class.
   */
  public function __construct() {
    $this->logger = new LoggerChannel("greeting_cards");
  }

}
