<?php

namespace Drupal\greeting_cards\Plugin;

/**
 * @file
 * For Internal Usage, this class will provide the functionality.
 */

/**
 * Class is designed to give access to internal uses of this module function.
 *
 * @class
 * Class to use outside this module
 */
class GreetingCard {
  /**
   * Image will be created from your pdf file given and id will be return.
   *
   * @var int ID of created image
   */
  private int $imageCreated;

  /**
   * Get the image id of just created image.
   *
   * @return int
   *   Image id in file_managed.
   */
  public function getImageCreated(): int {
    return $this->imageCreated;
  }

  /**
   * PDF path is required.
   *
   * @param string $pdfPath
   *   You can even pass drupal internal path ie public://.
   *
   * @throws \Exception
   */
  public function __construct(private readonly string $pdfPath) {
    if (!$this->validatePdf()) {
      throw new \Exception("File path is not of pdf");
    }
    $this->makeImage();
  }

  /**
   * True if its pdf uploaded or false if not.
   *
   * @return bool
   *   If file is truly pdf return true if not false
   */
  private function validatePdf(): bool {
    // Check if the file exists.
    if (!file_exists($this->pdfPath)) {
      return FALSE;
    }

    // Open the file.
    $handle = fopen($this->pdfPath, 'rb');

    // Check if the file could be opened.
    if (!$handle) {
      return FALSE;
    }

    // Read the first five bytes, which should be "%PDF-".
    $header = fread($handle, 5);

    // Close the file.
    fclose($handle);

    // Check if the header is "%PDF-".
    return ($header === "%PDF-");

  }

  /**
   * Creating image from pdf using function defined in .module file.
   *
   * @return void
   *   Nothing since it internally used
   */
  private function makeImage(): void {
    $this->imageCreated = greeting_cards_convert_pdf_to_image($this->pdfPath);
  }

  /**
   * Image id will be returned this id is of file_managed table.
   *
   * @param string $pdfFilePath
   *   Pdf file path.
   *
   * @return int
   *   Image created id in file_managed table
   *
   * @throws \Exception
   */
  public static function makeThumbnail(string $pdfFilePath): int {
    return (new static($pdfFilePath))->getImageCreated();
  }

}
