# Greeting Cards

The module has been created with the intention of easy creation of greeting
cards
thumbnails of PDFs that are designed to be printed out and folded
into a greeting card


This module is the best suitable for those who want to create image from pdf
front page.
And use these images as they want.

Greeting Card module comes with this built in content Type that will store your
images and pdf properly.

Greeting Card module also has E-Cards feature that will enable users
to create cards
and send them to their loved ones. The cards created through this way
the receiver of card can visit this card on site and further share it
to social media.
Facebook, Twitter, etc. also there can preview and download the pdf to fold it.

Greeting cards module with E-cards feature users are not just going
to upload pdf but also images
which will be joined to made to pdf with first page as its thumbnail image.

E-card has the capability to create multiple cards per upload.


## Table of contents

- Requirements
- Installation
- Configuration
- Troubleshooting
- FAQ
- Managements
- E-Cards
- Maintainers


## Requirements

This module requires the following modules:
- [Entity Reference Revisions](https://www.drupal.org/project/entity_reference_revisions)
- [mPDF](https://packagist.org/packages/mpdf/mpdf)
- [ImageMagick](https://imagemagick.org/script/download.php)
- Make sure you have your mailing system set up in your drupal site.
  as E-card requires it.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Configure the content type form display Administration > Structure > Content
   types > Greeting Cards
3. You have to drag PDF and Category to enabled section.
4. Make sure ImageMagick policy is configured correct to allow pdf to image
   creations.

> ImageMagick policy
>
> Locate the policy.xml file: The policy file is typically found
> in the /etc/ImageMagick-6 or /etc/ImageMagick-7 directory.
> The exact path may vary depending on your system.
>
> Edit the policy.xml file: Open the policy.xml file in a text editor.
> You may need superuser privileges to make changes. For example,
> you can use the sudo command on Linux:
>
> OR
>
> Linux users
>
> ```shell
> sudo nano /etc/ImageMagick-6/policy.xml
> ```

> Scroll down to find where policies are located and paste this policy
> if policy is not there already
>
> ```text
> <policy domain="coder" rights="read|write" pattern="PDF" />
> <policy domain="path" rights="none" pattern="@*" />
> ```

## FAQ

**Q: How do I create pdf thumbnail image**

**A:** During installation or enabling the module the Content Type
Greeting Cards was created you can use its form to upload PDF
and Category of Greeting card you are uploading the module will automatically
generate image for you.

**Q: Where can I see these images**

**A:** You can see these generated images via your site domain /printable-cards

**Q: Are these generated images downloadable**

**A:** Yes by visiting /printable-cards page you will find all generated
thumbnail layout out for you categorize with these respective pdf attached

## Managements
This module comes with these tools.
1. Routes
   - printable-cards — This is a public access route
   - greeting-cards/e-cards - This is a public access form for users
     to upload their pdf or images and send cards to friends.
2. Content Type.
   - This module comes with a content type of Greeting cards
     which you can access via an administrated panel.
   - This Content type has fields Category, Thumbnail image, pdf.
     You will need to enable Category and pdf fields.
3. Taxonomy
4. - This module comes with a taxonomy of Card Categories
     which you can fill up with terms manually, or you can configure
     field Category of a content type to create automatically if term not exits.

## E-Cards
E-cards creation can be done by exposing the form found
at /greeting-cards/e-cards.
This form is publicly accessible. All cards created
by this form will be part of Greeting Cards
Content type, therefore, can be managed as greeting cards.

- Since users will create cards to send to their friends via email,
  please make sure you have set up your site mail server as this
  feature requires it.
- mPdf library is also required.
- This feature as part of Greeting Cards it will also require Greeting cards
  Content type and Cards Categories taxonomy.

In Short,
if Greeting cards module installed successfully you don't need
to worry about Content Type and taxonomy,
therefore, you
only have to consider checking mPdf library installed
and mail settings of your drupal 8,9,10 site.


## Maintainers

- Chance Nyasulu - [chancenyasulu](https://www.drupal.org/u/chancenyasulu)
